import numpy as np
from bitstring import BitArray

# PROBLEM 10
# Solved problem already but inefficiently so
# I wanted to use a bit array to create a sieve
# which resulted in limited success

        
# create an array of primes up to a limit
def prime_array(limit) :
    # sieve
    ar1 = np.arange(2, limit)
    ar2 = np.arange(2,np.floor(np.sqrt(limit)))
    for i in ar2 :
        ar3 = [i**2 + n*i for n in ar1 if i**2 + n*i < limit]
        ar1 = [n for n in ar1 if not n in ar3]
    return ar1

def prime_bitarray(limit) :
    arr   = BitArray(limit)
    p_sum = 0
    for i in range(2, int(np.ceil(np.sqrt(limit)))) :
        if not arr[i] :
            print (i)
            p_sum = p_sum + i 
            arr.set(True, range(i*2, limit, i))
    print(arr)
    return sum(np.where(arr == False))

# determine if x is relatively prime to all in a
def rel_prime(a, x) :
    for i in a :
        if ((x % i) == 0) :
            return False
        if (i > np.sqrt(x)) :
            return True
    return True

def prime_sum(limit) :
    primes = []
    arr    = np.arange(2,limit+1)
    p_sum  = 0
    for i in arr :
        if (rel_prime(primes, i)) :
            primes = np.append(primes, i)
            p_sum  = p_sum + i
    print(sum(primes))
    return p_sum



#pa = prime_array(2000000)
print ("Answer: " + prime_sum(2000000))
