import numpy as np

# PROBLEM 7
print ("PROBLEM 7")
print ("=========")

# determine if x is relatively prime to all in a
def rel_prime(a, x) :
    for i in a :
        if ((x % i) == 0) :
            return False
        if (i > np.sqrt(x)) :
            return True
    return True

def nth_prime(n) :
    primes = []
    i = 2
    while (len(primes) < n + 1) :
        if (rel_prime(primes, i)) :
            primes = np.append(primes, i)
        i = i + 1
    return primes[n-1]

print ("Answer: %s\n\n" % nth_prime(10001))

