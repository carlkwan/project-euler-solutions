import numpy as np

# PROBLEM 9
print ("PROBLEM 9")
print ("=========")

# pythagorean triplets are of the form
#   a = mn
#   b = (m^2 - n^2)/2
#   c = (m^2 + n^2)/2
# for odd n, m with n < m 
def py_triplet() :
    interval = np.arange(1,999, 2)
    m_int = interval
    n_int = interval
    for m in m_int :
        n_int = [n for n in m_int if n < m]
        n_int = [n for n in n_int if n*m < 1000]
        n_int = [n for n in n_int if (m**2 - n**2)/2 < 1000]
        n_int = [n for n in n_int if (m**2 + n**2)/2 < 1000]
        for n in n_int :
            a = m*n
            b = (m**2 - n**2)/2
            c = (m**2 + n**2)/2
            if (a + b + c == 1000) :
                print ("a = " + str(a) + ", b = " + str(b) + ", c = " + str(c))
                return a*b*c
    return 0

print ("Answer: %s\n\n" % py_triplet())
