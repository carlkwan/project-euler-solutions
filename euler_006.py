import numpy as np

# PROBLEM 6
print ("PROBLEM 6")
print ("=========")

# sum a to b = sum 1 to b - sum 1 to a
def sum_lines(a,b) :
    return b*(b+1)/2 - a*(a+1)/2

# like above but with squares
def sum_squares(a,b) :
    return b*(b+1)*(2*b+1)/6 - a*(a+1)*(2*a+1)/6 

def sum_square_dif(a,b) :
    return (sum_lines(a,b))**2 - sum_squares(a,b)

print ("Answer: %s\n\n" % sum_square_dif(0,100))
