import numpy as np
from scipy.special import factorial

# PROBLEM 5
print ("PROBLEM 5")
print ("=========")

def smallest_multiple_pair(x, y) :
    u_lim = x*y
    for i in range(max(x, y), u_lim + 1) :
        if (i % x == 0 and i % y == 0) :
            return i

def smallest_multiple_interval(start, end) :
    end = end + 1
    if (start==1) :
        start = 2
    arr = np.arange(start, end)
    for i in arr :
        for j in arr :
            if (j % i == 0 and j != i) :
                arr = np.delete(arr, np.where(arr == i))
    u_lim = 1 
    for i in arr :
        u_lim = u_lim*i
    temp = arr[0]
    for a in arr :
        temp = smallest_multiple_pair(temp, int(a))
    return temp

print ("Answer: %s\n\n" % smallest_multiple_interval(1,20))
