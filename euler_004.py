import numpy as np

# PROBLEM 4
print ("PROBLEM 4")
print ("=========")

# check if a number is pali in its digits
def is_pali(x) :
    int_str = str(x)
    l = len(int_str)
    a = int(np.floor(len(int_str)/2))
    b = int(np.ceil(len(int_str)/2))
    front   = int_str[0:a]
    back    = int_str[b:len(int_str)]
    back    = back[::-1]
    return (front == back)

# brute force check all 3 digit x 3 digit numbers
def pali_prod() :
    a     = np.arange(999,99, -1)
    palis = []
    # could significant speed up by checking 
    # in reverse lexicographical order instead
    for i in a :
        for j in a :
            if (is_pali(int(i*j))) :
                palis = np.append(palis, int(i*j))
    return np.amax(palis)

print ("Answer: %s\n\n" % pali_prod())
