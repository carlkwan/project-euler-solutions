import numpy as np

# PROBLEM 2
print ("PROBLEM 2")
print ("=========")
def fib(n) :
    # note F_n = \lfloor \frac {\phi^n}{\sqrt 5} + 1/2 \rfloor
    phi = (1 + np.sqrt(5)) / 2
    return int(np.floor(np.power(phi, n) / np.sqrt(5) + 1/2))

def sum_fib_even(limit) :
    # odd + odd = even, odd + even = odd
    # => every 3-rd Fib is even (starting with 1, 1, 2)
    n = 0
    fib_n = 0
    fib_sum = 0
    while fib_n < limit :
        fib_sum = fib_sum + fib_n
        n = n + 3
        fib_n = fib(n)
    return fib_sum

print("Answer: %s\n\n" % sum_fib_even(4*(10**6)))


































