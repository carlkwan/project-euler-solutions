import numpy as np

# PROBLEM 3
print ("PROBLEM 3")
print ("=========")


def rel_prime(a, x) :
    for i in a :
        if ((x % i) == 0) :
            return False
    return True


def largest_prime_factor(x) :
    primes = np.array([])
    i = 2;
    prime_factors = [];
    while (i < x + 1) :
        if (rel_prime(primes, i)) :
            primes = np.append(primes, i)
            if (x % i == 0) :
                prime_factors = np.append(prime_factors, i)
                x = x/i
        i = i + 1
    return prime_factors[len(prime_factors)-1]

print ("Answer: %s\n\n" % largest_prime_factor(600851475143))

































