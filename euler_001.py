import numpy as np

# PROBLEM 1
print ("PROBLEM 1")
print ("=========")
def sum_mod_2(start, end, mod1, mod2) :
    a = sum(np.arange(start,end, mod1)) 
    b = sum(np.arange(start,end, mod2)) 
    c = sum(np.arange(start,end, mod1*mod2)) 
    return a + b - c

print ("Answer: %s\n\n" % sum_mod_2(0, 1000, 3, 5))


